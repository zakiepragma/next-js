// pages/index.js
import axios from "axios";
import {
  Box,
  Heading,
  Link,
  Table,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react";
import NextLink from "next/link";

export default function About({ posts }) {
  return (
    <Box>
      <Heading as="h1" size="xl" mb={4}>
        Posts
      </Heading>
      <Table>
        <Thead>
          <Tr>
            <Th>ID</Th>
            <Th>Title</Th>
            <Th>Actions</Th>
          </Tr>
        </Thead>
        <Tbody>
          {posts.map((post) => (
            <Tr key={post.id}>
              <Td>{post.id}</Td>
              <Td>
                <NextLink href={`/posts/${post.id}`}>
                  <Link>{post.title}</Link>
                </NextLink>
              </Td>
              <Td>
                <NextLink href={`/posts/${post.id}/edit`}>
                  <Link mr={2}>Edit</Link>
                </NextLink>
                <NextLink href={`/posts/${post.id}/delete`}>
                  <Link>Delete</Link>
                </NextLink>
              </Td>
            </Tr>
          ))}
        </Tbody>
      </Table>
    </Box>
  );
}

export async function getStaticProps() {
  const res = await axios.get("https://jsonplaceholder.typicode.com/posts");
  const posts = res.data;
  return {
    props: {
      posts,
    },
  };
}
